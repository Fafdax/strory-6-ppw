from django.test import TestCase
from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Peserta, Kegiatan
from .views import Story6_home, Story6_tambahPeserta, Story6_tambahKegiatan


# Create your tests here.
class TestUrlsViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.Story6_home_url = reverse("webs6:home")
        self.kegiatan = Kegiatan.objects.create(nama_kegiatan = "kegiatan satu")
        self.tambah_peserta_url = reverse("webs6:s6tp",args=[self.kegiatan.id])
        self.ada_peserta = Peserta.objects.create(nama_peserta="ada peserta")
        self.Story6_tambahKegiatan_url = reverse("webs6:s6tk")
        self.peserta_to_be_fafdax = Peserta.objects.create(nama_peserta="fafdax")

    
    def test_Story_home_url_GET(self): 
        response = self.client.get(self.Story6_home_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,"Story6_home.html")

    def test_Story6_tambahKegiatan_url_GET(self): 
        response = self.client.get(self.Story6_tambahKegiatan_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,"Story6_tambahKegiatan.html")
    
    def test_tambah_kegiatan_redirect_add_POST(self):
        response = self.client.post(self.Story6_tambahKegiatan_url, {
            "nama" : "nama kegiatan"
        })
        self.assertEquals(response.status_code,302)

    def test_tambah_peserta_POST_name_on_database(self):
        response = self.client.post(self.tambah_peserta_url, {
            "nama_peserta" : "ada peserta"
        })
        self.assertEquals(response.status_code,302)
        self.assertEquals(self.kegiatan.peserta.all()[0].nama_peserta,"ada peserta")
    
    def test_tambah_peserta_POST_tambah_nama(self):
        response = self.client.post(self.tambah_peserta_url, {
            "nama_peserta" : "test_nama"
        })
        self.assertEquals(response.status_code,302)
        self.assertEquals(self.kegiatan.peserta.all()[0].nama_peserta,"test_nama")




class TestModels(TestCase):
    def setUp(self):
        self.peserta_test = Peserta.objects.create(
            nama_peserta = "peserta new"
        )
        self.kegiatan_test = Kegiatan.objects.create(
            nama_kegiatan = "kegiatan new"
        )
        self.kegiatan_test.peserta.add(self.peserta_test)

    def test_nama_peserta(self):
        self.assertEquals(str(self.peserta_test),"peserta new")

    def test_nama_kegiatan(self):
        self.assertEquals(str(self.kegiatan_test),"kegiatan new")

