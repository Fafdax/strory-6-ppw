# Generated by Django 3.1.1 on 2020-10-24 14:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Peserta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_peserta', models.TextField(max_length=100, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Kegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_kegiatan', models.TextField(max_length=100, unique=True)),
                ('peserta', models.ManyToManyField(blank=True, to='webs6.Peserta')),
            ],
        ),
    ]
