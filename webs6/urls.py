from django.urls import path
from . import views

app_name = 'webs6'

urlpatterns = [
    path('', views.Story6_home, name = 'home'),
    path('tambahKegiatan/', views.Story6_tambahKegiatan, name = "s6tk"),
    path('tambahPeserta/<int:id>', views.Story6_tambahPeserta, name = "s6tp"),
]