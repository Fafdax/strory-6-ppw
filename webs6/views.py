from django.shortcuts import render, redirect, HttpResponse, get_object_or_404, reverse
from .forms import FormTambahPeserta, FormTambahKegiatan
from .models import Kegiatan, Peserta
from django.contrib import messages

# Create your views here.

def Story6_home(request):
    context = {
        "activities" : Kegiatan.objects.all(),
        "formPeserta" : FormTambahPeserta,
    }
    return render(request, 'Story6_home.html', context)

def Story6_tambahPeserta(request, id):
    if request.method == "POST":
        kegiatan = Kegiatan.objects.get(id=id)
        nama_baru = request.POST["nama_peserta"]

        peserta_baru, status_baru = Peserta.objects.get_or_create(nama_peserta = nama_baru)
        if status_baru:
            peserta_baru.save()
            kegiatan.peserta.add(peserta_baru)
            messages.add_message(request, messages.SUCCESS, "{} berhasil ditambahkan ke {}".format(nama_baru, kegiatan.nama_kegiatan))
        else:
            try:
                kegiatan.peserta.get(nama_peserta=nama_baru)
                messages.add_message(request, messages.WARNING, "{} sudah di kegiatan {}".format(nama_baru, kegiatan.nama_kegiatan))
            except:
                kegiatan.peserta.add(peserta_baru)
                messages.add_message(request, messages.SUCCESS, "{} berhasil ditambahkan ke {}".format(nama_baru, kegiatan.nama_kegiatan))
    return redirect(reverse("webs6:home"))

def Story6_tambahKegiatan(request):
    if request.method == "POST":
        keg_form = FormTambahKegiatan(request.POST)
        if keg_form.is_valid():
            keg_obj = keg_form.save(commit=False)
            keg_form.save()
        return redirect(reverse("webs6:home"))
    context = {
        "keg_form" : FormTambahKegiatan,
        "is_tambah_kegiatan" : True
    }
    return render(request, "Story6_tambahKegiatan.html", context)