from django.db import models

# Create your models here.


class Peserta(models.Model):
    nama_peserta = models.TextField(max_length = 100, unique=True)
    def __str__(self):
        return self.nama_peserta

class Kegiatan(models.Model):
    nama_kegiatan = models.TextField(max_length = 100, unique=True)
    peserta = models.ManyToManyField(Peserta,blank=True)
    def __str__(self):
        return self.nama_kegiatan
