from django import forms
from django.forms import ModelForm
from .models import Peserta, Kegiatan

class FormTambahPeserta(forms.ModelForm):
    nama_peserta = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control w-40",
        "placeholder" : "Nama Peserta"
    }), label = "")

    class Meta:
        model = Peserta
        fields = "__all__"

class FormTambahKegiatan(forms.ModelForm):
    nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs = {"class":"form-control w-50 text-center",
        "placeholder" : "Nama Kegiatan"
    }), label = "")

    class Meta:
        model = Kegiatan
        fields = "__all__"